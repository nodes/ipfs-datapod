#!/bin/bash
set -e

# Get the version number from package.json
version_tag=$(grep -oP '"version": "\K[^"]+' ./package.json)

# --- datapod
docker buildx build -f ./docker/Dockerfile -t duniter-datapod .

# Tag with version and 'latest'
docker image tag duniter-datapod h30x/duniter-datapod:$version_tag
docker image tag duniter-datapod h30x/duniter-datapod:latest

# Push both
docker image push h30x/duniter-datapod:$version_tag
docker image push h30x/duniter-datapod:latest

# # --- kubo
docker buildx build -f ./docker/Dockerfile.Kubo -t datapod-kubo .

# Tag with version and 'latest'
docker image tag datapod-kubo h30x/datapod-kubo:$version_tag
docker image tag datapod-kubo h30x/datapod-kubo:latest

# Push both
docker image push h30x/datapod-kubo:$version_tag
docker image push h30x/datapod-kubo:latest

# --- kubo-rpc
docker buildx build -f ./docker/Dockerfile.KuboRpc -t datapod-kubo-rpc .

# Tag with version and 'latest'
docker image tag datapod-kubo-rpc h30x/datapod-kubo-rpc:$version_tag
docker image tag datapod-kubo-rpc h30x/datapod-kubo-rpc:latest

# Push both
docker image push h30x/datapod-kubo-rpc:$version_tag
docker image push h30x/datapod-kubo-rpc:latest

# --- hasura
docker buildx build -f ./docker/Dockerfile.Hasura -t datapod-hasura .

# Tag with version and 'latest'
docker image tag datapod-hasura h30x/datapod-hasura:$version_tag
docker image tag datapod-hasura h30x/datapod-hasura:latest

# Push both
docker image push h30x/datapod-hasura:$version_tag
docker image push h30x/datapod-hasura:latest