
## Import Cesium+ data

After exporting the data to json files with the Rust script from `v2s-datapod`.

```sh
# doImport()
# takes about 200 seconds (4 minutes)
time pnpm exec tsx src/scripts/cesium-plus-import.ts
```

You can then manually pin the cid according to the command output.

```sh
ipfs pin add -r bafyreie74jtf23zzz2tdgsz7axfrm4pidje43ypqn25v4gkdtfjbcj62km
```

This will make easier to insert this data in any AMT or other data structure.

```sh
# doAllCplusCidsToAMT()
# takes about 180 seconds
time pnpm exec tsx src/scripts/cesium-plus-import.ts
# bafyreiffn3kkrakf7qj5m3wepsoxjwcojmesfyoexsvsovx23nhbfqu6bq
```