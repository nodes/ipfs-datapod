# Config

We will explore here how the datapod configures itself and its ipfs node in a production environment.

## Kubo node config

Kubo docker image contains a `configure.sh` script in its `/container-init.d/` directory. This script generates the keys needed by the indexer, named with a `dd_` prefix. Then it configures the network and gateway.

## Datapod bootstrap

To start from scratch, the datapod can directly synchronize from an other trusted tree. This can be configured with the `DATAPOD_BOOT` env var which can be:
- a merkle tree root CID
- an IPNS entry pointing to the latest root CID known by the given peer

TODO: replace this by a trusted peer list managed by the datapod admin with hardcoded defaults.