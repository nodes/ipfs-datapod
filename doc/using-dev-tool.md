## Using dev tool

The Duniter Datapod dev tool is a Vue app that helps understand how the system works, explore the data, and debug the indexer.

```sh
pnpm dev # for Vue dev tool UI
```