#!/bin/sh
set -ex

# --- peering ---
# add known peers (poka, hugo, aya)
ipfs config Peering.Peers --json '[
  {
    "ID": "12D3KooWEaBZ3JfeXJayneVdpc71iUYWzeykGxzEq4BFWpPTv5wn",
    "Addrs": ["/dns/gateway.datapod.ipfs.p2p.legal/tcp/4001"]
  },
  {
    "ID": "12D3KooWFp4JsoDo5FX8CFLtyJjaWWRZ8q3gr8uT2s9To2GYzRNA",
    "Addrs": ["/dns/datapod.coinduf.eu/tcp/4001"]
  },
  {
    "ID": "12D3KooWJnzYzJBtruXZwUQJriF1ePtDQCUQp4aNBV5FjpYVdfhc",
    "Addrs": ["/dns/ipfs.asycn.io/tcp/4001"]
  },
  {
    "ID": "12D3KooWQdw3ptcSk1exiBTBDGbFzLEhoVT3zyeoU1zrpswb3qyL",
    "Addrs": ["/dns/datapod.gyroi.de/tcp/4441"]
  }
]
'
# configure as a dht to avoid unnecessary http routing
ipfs config Routing.Type dht
