#!/bin/sh
set -ex

# --- IPNS ---
# use pubsub for IPNS records
# ipfs config --json Ipns.UsePubsub true
# republish records frequently
ipfs config --json Ipns.RepublishPeriod '"5m"'