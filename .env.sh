#!/bin/sh
# exports .env variables to be used by an other process
set -a
source ./.env
set +a