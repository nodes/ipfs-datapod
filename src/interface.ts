import type { CID } from 'multiformats'
import { kubo } from './kubo'
import { emptyInode, type IndexInode, type IndexLeaf } from './types'
import { BASE, KEYSIZE } from './consts'
import assert from 'assert'

// interface to easily iterate over the AMT and get all CIDs inside leaves
export async function* getAll(cid: CID): AsyncIterable<IndexLeaf> {
  const result = await kubo.dag.get(cid).catch((e) => {
    console.log('🕰 could not retreive ' + cid)
    console.log(e)
    return null
  })
  if (result == null) {
    // could not retreive this node, we are done iterating and return an emtpy array of CID
    return { key: "", val: [] }
  }
  const node = result.value
  if (node.leaf) {
    yield node as IndexLeaf
  } else {
    for (let b of node.children) {
      if (b) {
        for await (const item of getAll(b[1])) {
          yield item
        }
      }
    }
  }
}

// interface to easily iterate over diff of two AMT
// similar to "compareInodes" below, but focused on what's new in cid2 and iterate all the way down to the index requests
// iterate over all CIDs of index requests
export async function* getDiff(cid1: CID, cid2: CID): AsyncIterable<IndexLeaf> {
  const [node1, node2] = await Promise.all([kubo.dag.get(cid1), kubo.dag.get(cid2)]) // .catch() can not compare when can not retreive
  const [leaf1, leaf2] = [node1.value as IndexLeaf, node2.value as IndexLeaf]
  const [inode1, inode2] = [node1.value as IndexInode, node2.value as IndexInode]
  if (leaf1.leaf && leaf2.leaf) {
    const [added1, added2] = compareLeafs('', leaf1, leaf2, [], [])
    // only look on what was added in cid2
    if (added1.length != 0) {
      // console.debug('ignoring missing index request ' + added1)
    } else {
      // we put in leaf only the ones that were added
      yield { key: leaf1.key, leaf: added2.map(([_k, v]) => v) }
    }
  } else if (inode1.children && inode2.children) {
    // do the inode comparison
    yield* getDiffInodes(inode1, inode2)
  } else {
    throw Error('can only compare inodes or leaves with themselves')
  }
}

// same as getDiff, but operates on index internal nodes only, not cids that can represent something else
async function* getDiffInodes(inode1: IndexInode, inode2: IndexInode): AsyncIterable<IndexLeaf> {
  assert(inode1.ctx == inode2.ctx)
  const ctx = inode1.ctx
  // iterate over nodes children
  for (let i = 0; i < BASE; i++) {
    // ======== simple case ========
    // get left and right entries
    const li = inode1.children[i]
    const ri = inode2.children[i]
    if (li == null && ri == null) {
      // do not compare if they are both null
      continue
    }
    if (li == null) {
      // right is not null and was added, yield all its children as new
      yield* getAll(ri![1])
      continue
    }
    if (ri == null) {
      // left is not null and was added, ignore
      // console.debug(`ignoring missing data at ${ctx}${li[0]}: ${li[1]}`)
      continue
    }

    // both buckets have items, unstructure them to get key and value
    const [lik, lic] = li
    const [rik, ric] = ri
    if (lic.toString() == ric.toString()) {
      // do not compare if the cid is the same
      continue
    }
    if (lik == rik) {
      // keys are the same and only content changed, dig deeper in both
      yield* getDiff(lic, ric)
      continue
    }

    // ======== recursive diff case ========
    // there is a key diff, we have to compare
    if (lik.length > rik.length && lik.startsWith(rik)) {
      // intermediate inode might have been added to the right
      // create virtual node then dig deeper in right
      const lvnode = emptyInode(ctx + rik)
      const b = parseInt(lik[rik.length], BASE)
      lvnode.children[b] = [lik.slice(rik.length), lic]
      const r = await kubo.dag.get(ric)
      yield* getDiffInodes(lvnode, r.value)
      continue
    }
    if (lik.length < rik.length && rik.startsWith(lik)) {
      // intermediate inode might have been added to the left
      // create virtual node then dig deeper in left
      const rvnode = emptyInode(ctx + lik)
      const b = parseInt(rik[lik.length], BASE)
      rvnode.children[b] = [rik.slice(lik.length), ric]
      const l = await kubo.dag.get(lic)
      yield* getDiffInodes(rvnode, l.value)
      continue
    }

    // ======== simple diff case ========
    // keys do not cover the same time period
    // content is then completely different
    // ignore what's new in left as removed
    // only yield what's new in right
    // console.log('ignoring removed value ' + lic)
    yield* getAll(ric)
    continue
  }
}

// // recursive comparison of inodes
// // the differences are added asynchronously in "addedLeft" and "addedRight" arrays
// // meaning that these arrays will still grow after the function returns
// export async function compareInodes(
//   // key of compared inodes, used as termination condition to know if we reached the leaf
//   k: string,
//   // reference node for comparison
//   left: IndexInode,
//   // node to compare with reference
//   right: IndexInode,
//   // list of nodes in left not present in right, (key,value) pairs
//   addedLeft: Array<[string, CID]>,
//   // list of nodes in right not present in left, (key,value) pairs
//   addedRight: Array<[string, CID]>
// ) {
//   // termination condition, since we know the size of the key
//   if (k.length == KEYSIZE) {
//     console.log('comparing leaf ' + k)
//     compareLeafs(k, left as unknown as IndexLeaf, right as unknown as IndexLeaf, addedLeft, addedRight)
//     return
//   }
//   // console.log('comparing node ' + k)

//   // iterate over nodes children
//   for (let i = 0; i < BASE; i++) {
//     // get left and right entries
//     const li = left.children[i]
//     const ri = right.children[i]
//     if (li == null && ri == null) {
//       // do not compare if they are both null
//       continue
//     }
//     if (li == null) {
//       // right is not null and was added
//       const nk = k + ri![0]
//       console.log('added right ' + nk)
//       addedRight.push([k, ri![1]])
//       continue
//     }
//     if (ri == null) {
//       // left is not null and was added
//       console.log('added left')
//       const nk = k + li![0]
//       addedLeft.push([nk, li![1]])
//       continue
//     }

//     // both buckets have items, unstructure them to get key and value
//     const [lik, lic] = li
//     const [rik, ric] = ri
//     if (lic.toString() == ric.toString()) {
//       // do not compare if the cid is the same
//       console.log('same ' + k + lik)
//       continue
//     }

//     if (lik == rik) {
//       // keys are the same and only content changed, dig deeper in both
//       const nk = k + lik
//       Promise.all([kubo.dag.get(lic), kubo.dag.get(ric)]).then((r) => {
//         const [lin, rin] = r
//         compareInodes(nk, lin.value, rin.value, addedLeft, addedRight)
//       })
//       continue
//     }

//     // there is a key diff, we have to compare
//     if (lik.length > rik.length && lik.startsWith(rik)) {
//       const nk = k + rik
//       console.log('diff ' + nk)
//       // intermediate inode might have been added to the right
//       // create virtual node then dig deeper in right
//       const lvnode = emptyInode(nk)
//       const b = parseInt(lik[rik.length], BASE)
//       lvnode.children[b] = [lik.slice(rik.length), lic]
//       kubo.dag.get(ric).then((r) => {
//         compareInodes(nk, lvnode, r.value, addedLeft, addedRight)
//       })
//       continue
//     }
//     if (lik.length < rik.length && rik.startsWith(lik)) {
//       const nk = k + lik
//       console.log('diff ' + nk)
//       // intermediate inode might have been added to the left
//       // create virtual node then dig deeper in left
//       const rvnode = emptyInode(nk)
//       const b = parseInt(rik[lik.length], BASE)
//       rvnode.children[b] = [rik, ric]
//       kubo.dag.get(ric).then((l) => {
//         compareInodes(nk, l.value, rvnode, addedLeft, addedRight)
//       })
//       continue
//     }
//     // keys do not cover the same time period
//     // content is then completely different
//     addedLeft.push([k + lik, lic])
//     addedRight.push([k + rik, ric])
//     continue
//   }
// }

// compare leaves by "eating" cids one by one
// based on the assumption that leaves are sorted
function compareLeafs(
  k: string,
  left: IndexLeaf,
  right: IndexLeaf,
  addedLeft: Array<[string, CID]>,
  addedRight: Array<[string, CID]>
): [Array<[string, CID]>, Array<[string, CID]>] {
  while (true) {
    const l = left.leaf.shift()
    const r = right.leaf.shift()
    if (l == undefined) {
      right.leaf.map((e) => {
        addedRight.push([k, e])
      })
      return [addedLeft, addedRight]
    }
    if (r == undefined) {
      left.leaf.map((e) => {
        addedLeft.push([k, e])
      })
      return [addedLeft, addedRight]
    }
    if (l == r) {
      continue
    }
    if (l < r) {
      addedLeft.push([k, l])
      right.leaf.unshift(r)
      continue
    }
    if (l > r) {
      addedRight.push([k, r])
      left.leaf.unshift(l)
      continue
    }
  }
}
