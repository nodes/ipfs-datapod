import { create } from 'kubo-rpc-client'
import type { KuboRPCClient } from 'kubo-rpc-client'
import { Agent } from 'http'

// env
export const KUBO_RPC = process.env.KUBO_RPC || 'http://127.0.0.1:5001'
export const KUBO_GATEWAY = process.env.KUBO_GATEWAY || 'http://127.0.0.1:8080'

function getKuboClientsNode() {
  // create a RPC HTTP client // TODO unix socket for optimization
  const kubo: KuboRPCClient = create({
    url: new URL(KUBO_RPC),
    agent: new Agent({
      maxSockets: 50000
    })
  })
  // create an other RPC client only for pubsub
  const kubo2: KuboRPCClient = create({
    url: new URL(KUBO_RPC),
    agent: new Agent({
      keepAlive: true, // to prevent UND_ERR_BODY_TIMEOUT
      keepAliveMsecs: 1000
      // maxSockets: 1,
      // timeout: 100
    })
  })

  return { kubo, kubo2 }
}

const getKuboClientsPlatform = getKuboClientsNode

export const { kubo, kubo2 } = getKuboClientsPlatform()
