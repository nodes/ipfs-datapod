import { create } from 'kubo-rpc-client'

// env
export const KUBO_RPC = 'http://127.0.0.1:5001'
export const KUBO_GATEWAY = 'http://127.0.0.1:8080'

export function getKuboClientsBrower() {
  return { kubo: create(KUBO_RPC), kubo2: null }
}

const getKuboClientsPlatform = getKuboClientsBrower

export const {kubo, kubo2} = getKuboClientsPlatform()