import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/kubo',
      name: 'kubo',
      component: () => import('../views/KuboView.vue')
    },
    {
      path: '/feed',
      name: 'feed',
      component: () => import('../views/FeedView.vue')
    },
    {
      path: '/index',
      name: 'index',
      component: () => import('../views/IndexView.vue')
    },
    {
      path: '/ipns',
      name: 'ipns',
      component: () => import('../views/IpnsView.vue')
    },
    {
      path: '/diff',
      name: 'diff',
      component: () => import('../views/DiffView.vue')
    },
    {
      path: '/io',
      name: 'io',
      component: () => import('../views/IoView.vue')
    },
    {
      path: '/cesium-plus',
      name: 'cesium-plus',
      component: () => import('../views/CplusView.vue')
    }
  ]
})

export default router
