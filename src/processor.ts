import { CID } from 'multiformats'
import { kubo } from './kubo'
import { BASE, KEYSIZE } from './consts'
import { emptyInode, emptyVinode } from './types'
import type { IndexInode, IndexLeaf, IndexVinode, IndexHist } from './types'
import { DD_TAMT_HIST_OPT, ddKeys } from './indexer/ipns'
import { uniqueby } from './utils'
import assert from 'assert'

// ===================== utils =====================

/// convert timestamp to key
export function timestampToKey(timestamp: number): string {
  return timestamp.toString(BASE).padStart(KEYSIZE, '0')
}

/// return bucket corresponding to given letter
export function bucket(letter: string): number {
  return parseInt(letter[0], BASE)
}

/// resolve main IPNS history
export async function resolveHist(): Promise<CID> {
  for await (const name of kubo.name.resolve(ddKeys.tamt_hist, { nocache: true })) {
    return CID.parse(name.slice(6))
  }
  throw Error('no history')
}

/// add cid as new value in history
export async function publishHistory(cid: CID): Promise<void> {
  resolveHist().then((lastHistCid) => {
    kubo.dag.get(lastHistCid).then((lastHist) => {
      const newHist: IndexHist = {
        last_history: lastHistCid,
        current_index: cid,
        number: lastHist.value.number + 1,
        timestamp: Date.now()
      }
      kubo.dag.put(newHist).then((histcid) => {
        kubo.name.publish(histcid, DD_TAMT_HIST_OPT)
      })
    })
  })
}

// ===================== main API =====================

// // UNUSED
// // example on how we would add cid to index
// async function addToIndex(cid: CID, indexRequest: IndexRequest) {
//   // at this point we might want to add a queue to avoid recomputing the whole tree at each new entry
//   // but for the moment we will not group entries and do re-compute the whole tree each time
//   //
//   // see this presentation for more info about the merkle tree
//   // https://polkadot-blockchain-academy.github.io/pba-book/substrate/storage/slides.html#/

//   for await (const name of kubo.name.resolve(ddKeys.root, { nocache: true })) {
//     // get root CID from IPNS
//     const rootCID = CID.parse(name.slice(6))
//     // insert request into it
//     const newRootCID = await insertKnownIndexRequest(rootCID, cid, indexRequest)
//     // pin new root CID recursively to be sure to have all data and to meet the "pinned only" reprovide strategy
//     kubo.pin.add(newRootCID, { recursive: true })
//     // publish the new root CID to IPNS
//     await kubo.name.publish(newRootCID, DD_TAMT_OPT)
//     // also update history
//     publishHistory(newRootCID)
//   }
// }

// /// simplest way to insert index request given its CID
// // rootCid: root node of the AMT to add the index request in
// // indexRequestCid: index request to add
// // returns the new root cid
// export async function insertIndexRequest(rootCid: CID, indexRequestCid: CID): Promise<CID> {
//   const [rootDag, indexDag] = await Promise.all([kubo.dag.get(rootCid), kubo.dag.get(indexRequestCid)])
//   const key = timestampToKey(indexDag.value.timestamp)
//   return processInode(rootDag.value, key, insertRequest(indexRequestCid))
// }

// /// same as above but with known request
// export async function insertKnownIndexRequest(
//   rootCid: CID,
//   indexRequestCid: CID,
//   indexRequest: IndexRequest
// ): Promise<CID> {
//   const key = timestampToKey(indexRequest.time)
//   const rootDag = await kubo.dag.get(rootCid)
//   return processInode(rootDag.value, key, insertRequest(indexRequestCid))
// }

// ===================== process function utils =====================

// /// function used to process node
// // /!\ this is suboptimal to return a CID, the node (or something else) should be returned instead
// // this would allow to write more function like ones that delete content
// // TODO change this
// export interface ProcessFunction {
//   // input is the input data at the place where we want to process
//   (input: null | IndexLeaf | IndexInode): Promise<CID>
// }

// // returns a process function suitable for processInode that simply inserts request in a leaf
// // WIP does not handle inserting to a node yet
// function insertRequest(indexRequestCid: CID): ProcessFunction {
//   return (input: null | IndexLeaf | IndexInode) => {
//     if (input == null) {
//       // in this case we want to create a new leaf
//       return processLeaf(emptyLeaf(), indexRequestCid)
//     } else {
//       // in this case we want to insert indexRequestCid in existing leaf
//       return processLeaf(input as IndexLeaf, indexRequestCid)
//     }
//   }
// }

// ===================== tree edit algorithm =====================

// /// process internal node
// /// insert the CID returned by `func` at the position given by `key`
// /// returns the CID of the resulting modified parent node
// export async function processInode(node: IndexInode, key: string, func: ProcessFunction): Promise<CID> {
//   // console.log("key: " + key)

//   // bucket
//   const b = bucket(key)
//   // if bucket is available, place leaf in it
//   if (node.children[b] === null) {
//     node.children[b] = [key, await func(null)]
//   } else {
//     // must share bucket with a node
//     const [k1, cid1] = node.children[b] as [string, CID]
//     const comp = compareKey(k1, key)

//     switch (comp.type) {
//       // enter
//       case resType.Child:
//         const e = comp as inRes
//         // console.log('enter "' + e.common + '" key "' + e.nk + '"')
//         const enterNode: IndexInode | IndexLeaf = (await kubo.dag.get(cid1)).value
//         const enterNodeAsLeaf = enterNode as IndexLeaf
//         const enterNodeAsInode = enterNode as IndexInode
//         if (enterNodeAsLeaf.leaf) {
//           // we can not enter a leaf at this stage
//           throw Error('should not enter a leaf, this should have been an end')
//         }
//         node.children[b] = [e.common, await processInode(enterNodeAsInode, e.nk, func)]
//         break

//       // an additionnal hierarchy level is required
//       case resType.Parent:
//         const p = comp as inRes
//         // console.log('enter "' + p.common + '" key "' + p.nk + '"')
//         const newiNode = emptyInode()
//         newiNode.children[p.b] = [p.nk, cid1]
//         const newiNodeCid = await func(newiNode)
//         node.children[b] = [p.common, newiNodeCid]
//         break

//       // reached dest
//       case resType.Same:
//         // console.log('end on ' + key)
//         const other = (await kubo.dag.get(cid1)).value
//         node.children[b] = [k1, await func(other)]
//         break

//       // diff found
//       case resType.Diff:
//         const c = comp as diffRes
//         // console.log('diff on "' + c.common + '" keys "' + c.nk1 + '" / "' + c.nk2 + '"')
//         const newNode = emptyInode()
//         newNode.children[c.b1] = [c.nk1, cid1]
//         newNode.children[c.b2] = [c.nk2, await func(null)]
//         const newNodeCid = (await kubo.dag.put(newNode)) as CID
//         node.children[b] = [c.common, newNodeCid]
//         break
//     }
//   }
//   // now that we have the new node save it and return
//   const newCid = (await kubo.dag.put(node)) as CID
//   console.log('new inode: ' + newCid.toString() + ' at ' + key)
//   return newCid
// }

// /// process leaf in the tree
// /// children have to be unique and ordered
// export async function processLeaf(node: IndexLeaf, val: CID): Promise<CID> {
//   if (!node.leaf.some((c) => c.toString() == val.toString())) {
//     // only insert if not already there (avoid duplicate)
//     node.leaf.push(val)
//     // ensure leaf order is predictible
//     node.leaf.sort((a, b) => (a.toString() < b.toString() ? -1 : 1))
//   }
//   const newCid = (await kubo.dag.put(node)) as CID
//   return newCid
// }

// ===================== merge algorithm =====================

/// wrapper for below function when merging a node which has a CID
export async function mergeInodesSyncCID(nodeACID: CID, nodeB: IndexVinode | IndexLeaf): Promise<[number, CID]> {
  // fail with small timeout since this data is supposed to be pinned locally
  // console.log('fetching ' + nodeACID)
  const nodeA = (await kubo.dag.get(nodeACID)).value // FIXME decrease this timeout without bug
  const newCID = await mergeInodesSync(nodeA, nodeB)
  // unpin old node CID if different
  // we do not mind if it was not pinned
  // WIP pin des not work well
  // if (nodeACID.toString() != newCID.toString()) kubo.pin.rm(nodeACID).catch(() => {})
  // no need to pin new node CID like:
  // kubo.pin.add(newCID)
  // this is already done with the pin option kubo.dag.put(cid, { pin: true })
  return newCID
}

/// merge internal nodes, synchronous implementation
// useful to merge trees
// returns the count of elements and a CID
export async function mergeInodesSync(
  nodeA: IndexInode | IndexLeaf,
  nodeB: IndexVinode | IndexLeaf
): Promise<[number, CID]> {
  const isAleaf = (nodeA as IndexLeaf).leaf != undefined
  const isBleaf = (nodeB as IndexLeaf).leaf != undefined
  const [leafA, leafB] = [nodeA as IndexLeaf, nodeB as IndexLeaf]
  // these are not internal nodes, but leaves, and we should merge them
  if (isAleaf && isBleaf) {
    // should only merge leaves with same key
    assert(leafA.key == leafB.key)
    const allCIDs = (nodeA as IndexLeaf).leaf.concat((nodeB as IndexLeaf).leaf)
    const newLeaf = arrayToLeaf(leafA.key, allCIDs)
    return kubo.dag.put(newLeaf).then((cid) => {
      // WIP pin des not work well
      // kubo.pin.add(cid).catch((_e) => console.log(`📌📌 could not pin newly created leaf ${cid}`))
      return [newLeaf.leaf.length, cid]
    })
  } else if (isAleaf || isBleaf) {
    throw Error('should not be possible, are keys same size?')
  }
  // if we are not yet at the leaf level, we can safely assume that nodeA and nodeB are indeed inodes
  const [noda, nodb] = [nodeA as IndexInode, nodeB as IndexVinode]
  assert(noda.ctx == nodb.ctx)
  const ctx = noda.ctx

  // process each bucket sequencially
  let itemCount = 0
  for (let b = 0; b < BASE; b++) {
    const nAcb = noda.children[b]
    const nBcb = nodb.children[b]
    if (nAcb == null && nBcb != null) {
      // we can concretize nodeB directly
      const [kB, childB] = nBcb
      const [count, childBCID] = await concretizeCid(childB)
      itemCount += count
      noda.children[b] = [kB, childBCID]
    } else if (nAcb != null && nBcb != null) {
      // both are non null
      const [kA, childA] = nAcb
      const [kB, childB] = nBcb
      const comp = compareKey(kA, kB)

      switch (comp.type) {
        // when keys are the same, this is a "enter", recurse
        case resType.Same:
          const [countS, nodeCID] = await mergeInodesSyncCID(childA, childB)
          itemCount += countS
          noda.children[b] = [kA, nodeCID]
          break

        // B is child of A (inside), we add a level of hierarchy
        case resType.Child:
          const e = comp as inRes
          // since B is child (longer key), A can only be an inode, not a leaf
          const newcNode = emptyVinode(ctx + e.common)
          newcNode.children[e.b] = [e.nk, childB]
          const [countC, mergec] = await mergeInodesSyncCID(childA, newcNode)
          itemCount += countC
          noda.children[b] = [e.common, mergec]
          break

        // B is parent of A, an additional hierachy level is required
        case resType.Parent:
          const p = comp as inRes
          const newiNode = emptyInode(ctx + p.common)
          newiNode.children[p.b] = [p.nk, childA]
          const [countP, mergep] = await mergeInodesSync(newiNode, childB)
          itemCount += countP
          noda.children[b] = [p.common, mergep]
          break

        // there is a diff
        case resType.Diff:
          const c = comp as diffRes
          const newNode = emptyInode(ctx + c.common)
          newNode.children[c.b1] = [c.nk1, childA]
          // here, this is suboptimal since we are forced to fetch childA to take into account its item count
          itemCount += await getItemCount(childA)
          const [countD, childBCID] = await concretizeCid(childB)
          itemCount += countD
          newNode.children[c.b2] = [c.nk2, childBCID]
          const newNodeCid = await kubo.dag.put(newNode).then((cid) => {
            // WIP pinning does not work well
            // kubo.pin.add(cid).catch((_e) => console.log(`📌📌 could not pin newly created node ${cid}`))
            return cid
          })
          noda.children[b] = [c.common, newNodeCid]
          break
      }
    } else {
      // nBcb is null, keep nAcb bucket untouched (might be null or not)
      if (nAcb == null) continue
      // if nAcb is non null, we have to get its item count
      const [_kA, childA] = nAcb
      itemCount += await getItemCount(childA) // suboptimal too
      continue
    }
  }
  // now that we have the new node, we can upload it and return its cid
  noda.count = itemCount
  // console.debug(itemCount)
  const nodaCID = kubo.dag.put(noda).then((cid) => {
    // WIP pinning does not work well
    // kubo.pin.add(cid).catch((e) => console.log(`📌📌 could not pin newly merged node ${cid}`))
    return cid
  })
  return Promise.all([itemCount, nodaCID])
}

// ===================== virtual nodes management =====================

/// convert array of key/value pairs to virtual inode (tree)
// opti: use a dichotomy search instead of iterating over all elements
export function arrayToVinode(ctx: string, array: Array<[string, CID]>): IndexVinode {
  // initialize empty virtual node
  const node = emptyVinode(ctx)
  // process each bucket in order since elements are ordered
  for (let b = 0; b < BASE; b++) {
    // initialize list of elements that should go in this bucket
    const subArray: Array<[string, CID]> = []
    // shift element from the array until an end condition is reached
    while (true) {
      const elt = array.shift()
      if (elt == undefined) {
        // we reached the end of the array
        break
      }
      const k = bucket(elt[0])
      if (k != b) {
        array.unshift(elt)
        // element does not belong to this bucket, go to next
        break
      }
      // element goes in the current bucket, add it
      subArray.push(elt)
    }
    // then process the elements in this bucket
    if (subArray.length > 0) {
      // not empty, take first and last keys
      const k1 = subArray.at(0)![0]
      const k2 = subArray.at(-1)![0]
      if (k1 == k2) {
        // if they are the same, all items have the same key, this is a leaf
        node.children[b] = [
          k1,
          arrayToLeaf(
            ctx + k1,
            subArray.map(([_k, v]) => v)
          )
        ]
        continue
      }
      // keys have the same size, so if they are not equal and not "enter", they are "diff"
      const c = compareKey(k1, k2) as diffRes
      const minimalSubArray: Array<[string, CID]> = subArray.map(([k, v]) => [k.slice(c.common.length), v])
      node.children[b] = [c.common, arrayToVinode(ctx + c.common, minimalSubArray)]
    }
  }
  return node
}

/// transform array of cid to leaf object
export function arrayToLeaf(key: string, array: CID[]): IndexLeaf {
  const cidListUniqueSorted = uniqueby<CID>(array, (k) => k.toString()).sort((a, b) =>
    a.toString() < b.toString() ? -1 : 1
  )
  return { key, leaf: cidListUniqueSorted }
}

/// concretize virtual node to CID
export async function concretizeCid(node: IndexVinode | IndexLeaf): Promise<[number, CID]> {
  const nodeL = node as IndexLeaf
  if (nodeL.leaf) {
    return Promise.all([nodeL.leaf.length, kubo.dag.put(node) as Promise<CID>])
  }
  const newNode = await concretize(node as IndexVinode)
  // console.debug(newNode.count)
  return Promise.all([newNode.count, kubo.dag.put(newNode) as Promise<CID>])
}

/// concretize virtual node
async function concretize(node: IndexVinode): Promise<IndexInode> {
  let itemCount = 0 // count total items of nodes
  const childrenPromise: Array<null | Promise<[string, CID]>> = node.children.map((c) => {
    if (c == null) {
      return null
    }
    const [k, v] = c
    return concretizeCid(v).then(([count, cid]) => {
      itemCount += count // increment item count
      return [k, cid as CID]
    })
  })
  // note: we must await before referencing the variable itemCount in return value
  const awaitedChildrens = await Promise.all(childrenPromise)
  // console.debug(itemCount)
  const concretizedInode: IndexInode = {
    children: awaitedChildrens,
    count: itemCount,
    ctx: node.ctx
  }
  return concretizedInode
}

/// get item count contained in a given CID
async function getItemCount(cid: CID): Promise<number> {
  const node = (await kubo.dag.get(cid)).value
  const nodeL = node as IndexLeaf
  const nodeN = node as IndexInode
  if (nodeL.leaf) {
    return nodeL.leaf.length
  }
  if (nodeN.children) {
    // console.debug(nodeN.count)
    return nodeN.count
  }
  throw Error('can not get item count of this object: ' + cid)
}

// ===================== key comparison =====================

/// result where key differ at some point
export interface diffRes {
  /// type of result
  type: resType
  /// common part of keys
  common: string
  /// bucket of key 1
  b1: number
  /// rest of key 1
  nk1: string
  /// bucket of key 2
  b2: number
  /// rest of key 2
  nk2: string
}

/// result where key 1 is included in key 2 start
export interface inRes {
  /// type of result
  type: resType
  /// common part (key 1)
  common: string
  /// bucket for key 2
  b: number
  /// rest of key 2
  nk: string
}

/// result where keys are the same
export interface sameRes {
  /// type of result
  type: resType
}

/// type alias for comparison result
export type compResult = diffRes | inRes | sameRes
/// possible types for compResult
export enum resType {
  /// k1 == k2
  Same,
  /// k1 and k2 cover disjoint sections
  Diff,
  /// k2 is child of k1 (k1 + nk)
  Child,
  /// k1 is child of k2 (k2 + nk)
  Parent
}

/// compare keys and return comp result
export function compareKey(k1: string, k2: string): compResult {
  // if keys are the same
  if (k1 == k2) {
    return {
      type: resType.Same
    }
  }
  // start comparison
  let common = ''
  const l = Math.min(k1.length, k2.length)
  for (let i = 0; i < l; i++) {
    const c1 = k1[i]
    const c2 = k2[i]
    if (c1 == c2) {
      common += c1
    } else {
      // return the comparison result
      return {
        type: resType.Diff,
        common,
        b1: bucket(c1),
        nk1: k1.slice(common.length),
        b2: bucket(c2),
        nk2: k2.slice(common.length)
      }
    }
  }
  // we reached the end of the shortest key without diff
  if (k1.length < k2.length) {
    return {
      type: resType.Child,
      common,
      b: bucket(k2[l]),
      nk: k2.slice(common.length)
    }
  } else {
    return {
      type: resType.Parent,
      common,
      b: bucket(k1[l]),
      nk: k1.slice(common.length)
    }
  }
}
