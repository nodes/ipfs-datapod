import { CID } from 'multiformats'
import { wrapCplusInIndexRequest, indexRequestsToAMT } from '../cesium-plus'
import { createInterface } from 'readline'
import { appendFile, createReadStream } from 'fs'
import { timestampToKey } from '../processor'
import { readFile } from 'fs/promises'
import { EMPTY_NODE_CID } from '../consts'

// fetch raw profile
// fetchRawCplus('38QzVPhRLbEiqJtvCmRY6A6SraheNA6fJbomFX75b2qb').then(console.log)
async function fetchRawCplus(id: string): Promise<string> {
  const ENDPOINT = 'https://g1.data.e-is.pro'
  return fetch(ENDPOINT + '/user/profile/' + id + '/_source').then((b) => b.text())
}

/// download all c+ data and add them to a file
async function downloadAllCplusProfilesRaw(endpoint: string, filename: string) {
  const SCROLL_TIME = '5m'
  const PAGE_SIZE = 100
  const ENDPOINT = endpoint
  const URL = `${ENDPOINT}/user/profile/_search?scroll=${SCROLL_TIME}&size=${PAGE_SIZE}`
  const NOTIF = 1000

  // first batch
  let batch = await fetch(`${URL}&filter_path=_scroll_id,hits.total,hits.hits._id`, {
    method: 'post',
    body: JSON.stringify({
      query: { match_all: {} }
    })
  }).then((b) => b.json())
  let scroll_id = batch._scroll_id
  const total = batch.hits.total
  let scrolled = PAGE_SIZE

  console.log(`downloading ${total} cplus profiles...`)

  // process batches while available
  while (scrolled < total) {
    // add raw source to the file
    for (const hit of batch.hits.hits) {
      fetchRawCplus(hit._id).then((cplusRaw) => appendFile(filename, cplusRaw + '\n', () => {}))
    }
    // take next batch
    batch = await fetch(ENDPOINT + '/_search/scroll', {
      method: 'post',
      body: JSON.stringify({
        scroll: SCROLL_TIME,
        scroll_id: scroll_id
      })
    }).then((b) => b.json())
    scroll_id = batch._scroll_id
    scrolled += PAGE_SIZE
    if (scrolled % NOTIF == 0) {
      console.log(`${scrolled.toString().padStart(5)}/${total}`)
    }
  }
  console.log(`${total}/${total}, done.`)
}

/// put all raw cplus profiles to ipfs node in index request and write result to a file
async function wrapRawProfilesInIndexRequest(input: string, output: string) {
  const TIMESTAMP_MULTIPLIER = 1000 // cesium plus data was using seconds instead of milliseconds
  const LIMIT = 500 // max number of lines to process simultaneously
  const NOTIF = 2000 // log every N lines processed
  const rejected = './input/cplusHS.txt'
  const convertImg = false // also upload base64 image as separate file for later reference
  let queueSize = 0
  let readTotal = 0

  await new Promise<void>((resolve, reject) => {
    // line process function
    function process(line: string) {
      queueSize++
      if (queueSize > LIMIT) {
        linereader.pause()
      }
      try {
        const cplus = JSON.parse(line)
        wrapCplusInIndexRequest(cplus, line, convertImg)
          .then((cid) => timestampToKey(cplus.time * TIMESTAMP_MULTIPLIER) + ' ' + cid.toString() + '\n')
          .then((l) =>
            appendFile(output, l, () => {
              readTotal++
              queueSize--
              if (queueSize < LIMIT) {
                linereader.resume()
              }
              if (readTotal % NOTIF == 0) {
                console.log(`processed ${readTotal} profiles`)
              }
            })
          )
          .catch((e) => {
            console.log(e)
            appendFile(rejected, line, () => {
              readTotal++
            })
          })
      } catch (e) {
        appendFile(rejected, line + '\n\n\n', () => {})
      }
    }
    // stream
    const linereader = createInterface(createReadStream(input))
    linereader.on('line', process)
    linereader.on('close', resolve)
    linereader.on('error', reject)
  })

  console.log('done.')
}

// expects to receive a file with on each line a label and the index request CID
async function importIrToAMT(rootNodeCid: CID, input: string) {
  const requests = await readFile(input, 'utf8')
    .then((r) => r.split('\n'))
    .then((p) =>
      p
        .filter((e) => e.length > 0)
        .map((e) => {
          const parts = e.split(' ')
          return [parts[0], CID.parse(parts[1])] as [string, CID]
        })
    )
    .then((l) => l.sort())
  await indexRequestsToAMT(requests, rootNodeCid)
}

async function main() {
  console.log(Date.now(), 'start downloading cplus profiles')

  // 26 minutes
  // this can take a while because ~50000 profiles are downloaded in raw format independantly
  // 'https://g1.data.e-is.pro'
  await downloadAllCplusProfilesRaw('https://g1.data.e-is.pro', './input/cplusimport.jsonl')

  console.log(Date.now(), 'start wrapping in index requests')

  // 12 minutes
  // speed is reduced to limit RAM usage and concurrent writes to IPFS node
  await wrapRawProfilesInIndexRequest('./input/cplusimport.jsonl', './input/cplusIR.txt')

  console.log(Date.now(), 'start adding to current root cid')

  // 3 minutes
  // import by batch and logs successive cids

  // import to an empty node
  // importIrToAMT(EMPTY_NODE_CID, './input/cplusIR.txt')

  // import dev index requests on an existing root
  // const rootCID = CID.parse('bafyreieybuh6l6bpz3jn76wqbf7jweb4ptq55n3avbaxe3nhkeiabxzmze')
  // importIrToAMT(rootCID, './input/devIr+labels.txt')

  // import all cplus data on an existing root
  // root CID we want to import to (example: current root CID of datapod → "tamt")
  const rootCID = CID.parse('bafyreih5fz46ezyf25jns6azxxjrjr625f2hvkt24zj4w7wm3dfedfwgv4')
  await importIrToAMT(rootCID, './input/cplusIR.txt')
  // latest log is the new root cid to start the indexer on
  // pnpm start /ipfs/<root cid>

  console.log(Date.now(), 'finished. Now start your indexer.')
}

main()
