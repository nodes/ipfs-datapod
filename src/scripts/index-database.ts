import { CID } from 'multiformats'
import { client, handleInsertRequest } from '../indexer/database'
import { getAll } from '../interface'
import { kubo } from '../kubo'
import type { IndexRequest } from '../types'

const cid = CID.parse('bafyreiay2zpectyuxb4d5nxxkwcpdk76ivrm36qmrutpbaaxldh6yt46xi')
// await client.end()


// // example to insert a specific request
// const irCID = CID.parse('bafyreifpzcrghku22vp5oqzqgj4v3hxfhewe5d23kqdkhnkfapnrjgdoje')
// const ir = await kubo.dag.get(irCID)
// await handleInsertRequest(irCID, ir.value)
