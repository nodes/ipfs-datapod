import { EMPTY_NODE_CID } from '../consts'
import { getDiff } from '../interface'
import { CID } from 'multiformats'
import { setTimeout } from 'timers/promises'

console.log('start')

// const fromCID = EMPTY_NODE_CID
// const fromCID = CID.parse('bafyreic6qy2k5w6324uayfzoybmzypdv57zk3zxezaiws4h553jjogw6o4')
// const toCID = CID.parse('bafyreihls2kmwx2ufuwx4kbl67f3ipl5wbc6j6snfegy3sttymrhxsgvpa')
// const fromCID = CID.parse('bafyreiaixvejxrcszzexohdo5obtduw5mctvti2jsgob4pnq7ovd5ngrxi')
// const toCID = CID.parse('bafyreiel7fh42ehswlh7wg4mz5zzrugwbpuevzojxuzw3dwgyoiyhvjhma')
// const fromCID = CID.parse('bafyreifqojtso7fkz2qg3d5p432jbcjgzah2bmvcvowfhbvn4dowghlq2a')
// const toCID = CID.parse('bafyreiel7fh42ehswlh7wg4mz5zzrugwbpuevzojxuzw3dwgyoiyhvjhma')
// const toCID = CID.parse('bafyreihwmnq3wtfgbe7mlbwkztasqfcr5auopxwgmw6bnmobhfccgolayu')

const fromCID = CID.parse("bafyreidnn24efeospk67qbaj7wipug3ewkqzleniczt4tsdsevplz5ynwe") // ok
const toCID = CID.parse("bafyreiaazdjy2zslvpwnnwzcum2dwzsjk7x72lwssqxmkjkzdkxyaiszpi") // bad count

const iterator = getDiff(fromCID, toCID)
// const iterator = getDiff(toCID, fromCID)

const BATCH_SIZE = 1000

async function doit() {
  let num = 0
  for await (const item of iterator) {
    for (let irCID of item.leaf) {
      num += 1
      if (num % BATCH_SIZE == 0) {
        console.log(num)
      }
      await setTimeout(1000) // wait 1 sec

      console.log(item.key, irCID.toString())
    }
  }
}

doit()
