import { arrayToVinode, mergeInodesSyncCID } from '../processor'
import { kubo } from '../kubo'
import type { CID } from 'multiformats'
import { emptyInode } from '../types'
import { getAll } from '../interface'

const obj = { index: 'request', fake: true }
const fakeRoot = emptyInode('fake_')

// I should transform that into a unit test

async function main() {
  const rootCID = await kubo.dag.put(fakeRoot)
  const cid = await kubo.dag.put(obj)

  const items: Array<[string, CID]> = [
    ['123', cid],
    ['456', cid],
    ['456', cid]
  ]

  const root = arrayToVinode('fake_', items)
  console.log(root)
  console.log(JSON.stringify(root))

  const [_, res] = await mergeInodesSyncCID(rootCID, root)
  console.log(res)

  const collect = []
  for await (const item of getAll(res)) {
    collect.push(item)
  }

  console.log(collect)
}

main()
