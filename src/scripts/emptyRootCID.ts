import { EMPTY_NODE_CID } from '../consts'
import { kubo } from '../kubo'
import { concretizeCid } from '../processor'
import { emptyRootInode, emptyVinode } from '../types'

const node = emptyRootInode()
kubo.dag.put(node).then((cid) => console.log('node cid', cid))

const vnode = emptyVinode('')
concretizeCid(vnode).then(([_, vcid]) => console.log('vnode cid', vcid))

console.log('empty node cid', EMPTY_NODE_CID)
