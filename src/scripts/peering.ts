import { RoutingEventTypes, type RoutingFinalPeerEvent } from 'kubo-rpc-client'
import { kubo } from '../kubo'

// tests resolution of peers to prototype peer discovery

async function doit() {
  const peers = await kubo.swarm.peers()
  for (const p of peers) {
    for await (const evt of kubo.routing.findPeer(p.peer)) {
      if (evt.type == RoutingEventTypes.FINAL_PEER) {
        const e = evt as RoutingFinalPeerEvent
        for (const a of e.peer.multiaddrs) {
          const astr = a.toString()
          if (astr.startsWith('/dns')) {
            console.log(astr)
          }
        }
      }
    }
  }
}

doit()
