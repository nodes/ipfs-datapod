import { decodeAddress, signatureVerify } from '@polkadot/util-crypto'
import { u8aToHex } from '@polkadot/util'
import { CID } from 'multiformats'
import { kubo } from './kubo'
import type { IndexRequest } from './types'
import type { Message, SignedMessage } from '@libp2p/interface'
import assert from 'assert'

// 1 minute in milliseconds
const MAX_IR_TIME_DIFF = 1000 * 60

// === payload verification

// build payload to sign
export function buildStringPayload(ir: IndexRequest) {
  let payload = `prefix: Duniter Datapod
time: ${ir.time}
kind: ${ir.kind}
`
  if (ir.data)
    payload += `data: ${ir.data.toV1()}
`
  return payload
}

// check if signature is valid // TODO remove polkadot dependency
export function isValidSignature(signedMessage: string, signature: string, address: string) {
  const publicKey = decodeAddress(address)
  const hexPublicKey = u8aToHex(publicKey)
  return signatureVerify(signedMessage, signature, hexPublicKey).isValid
}

// === message handling

// returns a pubsub message handler
// TODO allow more control on what to do from invalid message (instead of simple logging)
//   so that it could be used by the app message handler
// TODO allow more control on what to do before processing valid messages (filtering, trust list...)
//   this allows to give more details on what is considered valid
export function getPubSubHandler(
  validMessageHandler: (cid: CID, dag: IndexRequest) => Promise<void>
): (message: Message) => Promise<void> {
  return async (message) => {
    // 1. check that message is signed
    if ((message as SignedMessage).signature == undefined) {
      console.log('received unsigned message ' + JSON.stringify(message))
      return
    }
    // 2. cast as signed message
    message = message as SignedMessage
    // 3. decode message as text
    const msg = new TextDecoder().decode(message.data).trim()

    try {
      // the message must be a cid
      const cid = CID.parse(msg)
      // logs the peer for debugging
      const peer = message.from
      console.debug('✉️  received cid ' + cid + ' from ' + peer)
      // get message
      kubo.dag
        .get(cid)
        .then(function (d) {
          const dag = d.value
          // some validation on dag to ensure it is exact format
          // errors if format is not good
          const ir = dagAsIndexRequest(dag)
          // build payload to sign and check signature (null not allowed here)
          const stringPayload = buildStringPayload(ir)
          const isSigValid = isValidSignature(stringPayload, ir.sig!, ir.pubkey)
          if (isSigValid) {
            // at this point we could apply different treatment based on the key
            // we could for example have a trust list published in ipfs
            // this is mostly for spam prevention
            // but for the moment we will just assume that we trust any key and add the index request to the pool

            // we could also apply different treatment based on the key:
            // high trust => common timestamp AMT
            // low trust => sandboxed HAMT

            // call valid index request handler
            validMessageHandler(cid, ir).catch((e) => {
              console.log('error processing valid message')
              console.log(e)
            })

            // this is all that this indexer does
            // the rest (autopinning, postgres indexing... is be managed by an other part of the indexer)
          } else {
            console.log('🚫 invalid signature ' + msg)
          }
        })
        .catch((e) => {
          console.log('☁️  error with pubsub data  ' + cid)
          console.log(Date.now())
          console.log(e)
        })
    } catch {
      console.log('👾 invalid message ' + msg)
    }

    return
  }
}

// build index request from dag or throw error
function dagAsIndexRequest(dag: any): IndexRequest {
  // check fields
  // 1. kind must be a string with a length lower than 64
  assert(typeof dag.kind === "string")
  assert(dag.kind.length <= 64)
  // 2. time must be not too old (one minute for example)
  // should not be in the future
  // (prevents too much control on the storage key)
  assert(typeof dag.time === "number")
  assert(Date.now() - dag.time < MAX_IR_TIME_DIFF)
  // 3. data must be null or a cid
  if(dag.data != null) {
    assert(!!CID.asCID(dag.data))
  }
  // all checks passed, pubkey and signature are checked later on
  // extracts fields of interest from dag
  // note: extra fields are theoretically allowed but will not be taken into account and not saved
  return {
    kind: dag.kind,
    time: dag.time,
    data: dag.data,
    pubkey: dag.pubkey,
    sig: dag.sig
  }
}