import type { NamePublishOptions } from 'kubo-rpc-client'
import { kubo } from '../kubo'
import { type DdKeys } from './types'

// key names by kubo
export const DD_ROOT = 'dd_root'
export const DD_TAMT = 'dd_tamt'
export const DD_TAMT_HIST = 'dd_tamt_hist'
export const DD_PROFILES = 'dd_profiles'

// get keys of self node by name to find all IPNS entries
// the keys should be defined per node with the configure.sh script
// using dd_ prefix like dd_tamt, dd_tamt_hist...
export async function getSelfDdKeys(): Promise<DdKeys> {
  const ks = new Map((await kubo.key.list()).map((k) => [k.name, k.id]))
  const keys: DdKeys = {
    root: ks.get(DD_ROOT)!,
    tamt: ks.get(DD_TAMT)!,
    tamt_hist: ks.get(DD_TAMT_HIST)!,
    profiles: ks.get(DD_PROFILES)!
  }
  return keys
}

// make keys available for deps
export const ddKeys = await getSelfDdKeys()

// publish options
export const DD_TAMT_HIST_OPT: NamePublishOptions = { key: DD_TAMT_HIST, ttl: '1m', lifetime: '12h' }
export const DD_TAMT_OPT: NamePublishOptions = { key: DD_TAMT, ttl: '1m', lifetime: '24h' }
export const DD_ROOT_OPT: NamePublishOptions = { key: DD_ROOT, ttl: '24h', lifetime: '72h' }
