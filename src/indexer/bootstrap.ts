import type { CID } from 'multiformats'
import { kubo } from '../kubo'
import { DD_ROOT_OPT, DD_TAMT_HIST_OPT, getSelfDdKeys } from './ipns'
import { resolveHist } from '../processor'
import type { IndexHist } from '../types'

// setup the root index from scratch
// after that, an other node can setup its own root index from another peer
// this is a no-op if keys are already published, so it is fine to call it at each startup
// because it garantees that the keys in used are well published
export async function publishKeys() {
  const keys = await getSelfDdKeys()
  const cid = await kubo.dag.put(keys)
  await kubo.name.publish(cid, DD_ROOT_OPT)
}

/// check that history is available else initialize it
export async function initHistIfNull(cid: CID): Promise<void> {
  try {
    await resolveHist()
  } catch {
    // define first history element
    const firstHist: IndexHist = {
      last_history: null,
      current_index: cid,
      number: 0,
      timestamp: Date.now()
    }
    const firstHistCID = await kubo.dag.put(firstHist)
    await kubo.name
      .publish(firstHistCID, DD_TAMT_HIST_OPT)
      .then(() => console.log('initialize history to ' + DD_TAMT_HIST_OPT.key))
  }
  return
}

// TODO allow setting peer list from config
// dd_root of known indexers
export const TRUSTED_PEERS = [
  // hugo localhost dev indexer
  '/ipns/k51qzi5uqu5dip97g90ww9dxql8jc7t7sd0qi1fxx8g0rfr56vbt41tk17xnoe',
  // hugo coinduf.eu indexer
  '/ipns/k51qzi5uqu5djid2dlwsjecuri0yluo1yy1lzqkdvlthjljiaff28b38gf2399',
  // hugo gyroi.de indexer
  '/ipns/k51qzi5uqu5djeezax88gf4hd09i59umweoi2zvsd37w8jl95nfbuhru58i73l'
]
