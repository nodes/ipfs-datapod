import {
  CESIUM_PLUS_PROFILE_IMPORT,
  CESIUM_PLUS_PROFILE_INSERT,
  CESIUM_PLUS_PROFILE_DELETE,
  TRANSACTION_COMMENT,
  CESIUM_PLUS_PROFILE_DELETE_OLD,
  CESIUM_PLUS_PROFILE_INSERT_OLD,
  TRANSACTION_COMMENT_OLD,
  DD_USER_FS
} from '../consts'
import type { CplusProfile, IndexRequest, TxComment } from '../types'
import { CID } from 'multiformats'
import pg from 'pg'
import { kubo } from '../kubo'

// define form env
const env = {
  DB_HOST: process.env.DB_HOST || 'localhost',
  DB_PORT: parseInt(process.env.DB_PORT || '0') || 5432,
  DB_USER: process.env.DB_USER || 'postgres',
  DB_PASSWORD: process.env.DB_PASSWORD || 'my_db_password'
}

// group query and param builder to limit error
interface QueryBuilder {
  // SQL query
  query: string
  // data getter when there can be some manipulation with data CID
  dataGetter: (dataCID: CID) => Promise<any>
  // data transform before param builder
  dataTransform: (irCID: CID, ir: IndexRequest, dataCID: CID, data: any) => Promise<any>
  // build params for query
  paramBuilder: (irCID: CID, ir: IndexRequest, dataCID: CID, data: any) => any[]
}
const defaultDataGetter = (dataCID: CID) => kubo.dag.get(dataCID).then((d) => d.value)
const defaultDataTransform = (_a: CID, _b: IndexRequest, _c: CID, d: any) => Promise.resolve(d)

// initialize client
const { Client } = pg
export const client = new Client({
  host: env.DB_HOST,
  port: env.DB_PORT,
  user: env.DB_USER,
  password: env.DB_PASSWORD
})
await client.connect()

// functions

// get key in key/value meta
async function getKey(key: string): Promise<string | null> {
  const res = await client.query('SELECT value FROM meta WHERE key=$1', [key])
  if (res.rows.length != 0) {
    return res.rows[0].value
  } else {
    return null
  }
}
// set key in key/value meta
async function setKey(key: string, value: string) {
  await client.query(
    'INSERT INTO meta(key, value) VALUES ($1, $2) ON CONFLICT (key) DO UPDATE SET value = EXCLUDED.value',
    [key, value]
  )
}

// latest root cid that was indexed
export async function getLatestIndexedCID(): Promise<CID | null> {
  const val = await getKey('last_indexed_cid')
  return val ? CID.parse(val) : null
}
// set latest root cid
export async function setLatestIndexedCID(cid: CID) {
  await setKey('last_indexed_cid', cid.toString())
}

// query builder for user fs
const userFsQb: QueryBuilder = {
  query: `INSERT INTO
    user_fs(index_request_cid, time, pubkey, data_cid)
    VALUES ($1, $2, $3, $4,)
    ON CONFLICT (pubkey)
    DO UPDATE SET
      index_request_cid = EXCLUDED.index_request_cid,
      time = EXCLUDED.time,
      pubkey = EXCLUDED.pubkey, 
      data_cid = EXCLUDED.data_cid,
    WHERE EXCLUDED.time > profiles.time;
        `,
    // we do not *need* to fetch any data
    // however, we do fetch the first node to make sure it becomes available locally
    // this is the place where we could fetch more specific parts of this FS
    // to be sure to make them available on the network for later
    dataGetter: defaultDataGetter,
    dataTransform: defaultDataTransform,
    paramBuilder: (irCID: CID, ir: IndexRequest, dataCID: CID, _data: any) => [
      // $1 index_request_cid
      irCID.toString(),
      // $2 time
      new Date(ir.time).toISOString(),
      // $3 pubkey
      ir.pubkey,
      // $4 data_cid
      dataCID.toString(),
    ]
}

// cesium plus profile query and param builder
// completely overwrites previous data if new timestamp is higher
const cesiumPlusProfile: QueryBuilder = {
  query: `INSERT INTO
    profiles(index_request_cid, time, pubkey, data_cid, title, description, avatar, geoloc, city, socials)
    VALUES ($1, $2, $3, $4, $5, $6, $7, point($8, $9), $10, $11)
    ON CONFLICT (pubkey)
    DO UPDATE SET
      index_request_cid = EXCLUDED.index_request_cid,
      time = EXCLUDED.time,
      pubkey = EXCLUDED.pubkey, 
      data_cid = EXCLUDED.data_cid, 
      title = EXCLUDED.title,
      description = EXCLUDED.description,
      avatar = EXCLUDED.avatar,
      geoloc = EXCLUDED.geoloc,
      city = EXCLUDED.city,
      socials = EXCLUDED.socials
    WHERE EXCLUDED.time > profiles.time;
        `,
  dataGetter: defaultDataGetter,
  dataTransform: defaultDataTransform,
  paramBuilder: (irCID: CID, ir: IndexRequest, dataCID: CID, data: CplusProfile) => [
    // $1 index_request_cid
    irCID.toString(),
    // $2 time
    new Date(ir.time).toISOString(),
    // $3 pubkey
    ir.pubkey,
    // $4 data_cid
    dataCID.toString(),
    // $5 title (truncated to 128 chars)
    data.title.substring(0,128),
    // $6 description (truncated to 1024 chars, full version still available on IPFS)
    data.description.substring(0,1024),
    // $7 avatar (makes sure it is not more than a reasonable CID)
    data.avatar?.toString().substring(0,64),
    // $8 geoloc
    data.geoPoint?.lat,
    // $9
    data.geoPoint?.lon,
    // $10 city (truncated to 128 chars)
    data.city?.toString().substring(0,128),
    // $11 socials (json truncated to 4096 chars, full version available on IPFS, research in json is unlikely)
    data.socials ? JSON.stringify(data.socials).substring(0,4096) : undefined
  ]
}

// query builder for raw cplus data
const cesiumPlusProfileRaw: QueryBuilder = {
  // same query
  query: cesiumPlusProfile.query,
  // data is not stored directly as dag but as unix fs json
  dataGetter: async (dataCID: CID) => {
    const stream = kubo.cat(dataCID)
    const decoder = new TextDecoder()
    let str = ''
    for await (const chunk of stream) {
      str += decoder.decode(chunk)
    }
    return str
  },
  // transform data before indexing
  // here "data" is a dag-pb UnixFS
  dataTransform: async (irCID, ir, dataCID, data) => {
    const cplus: any = JSON.parse(data)
    const avatar: any = cplus.avatar
    // transform base64 avatar to CID if present
    if (avatar != undefined && avatar._content != undefined) {
      const buffer = Buffer.from(avatar._content, 'base64')
      const fileCandidate = { content: new Uint8Array(buffer) }
      // optimization: compute the hash locally without submitting it to kubo
      // difficulty: check that the hash is the same
      // FIXME adding the avatar like this causes a lot of computation
      cplus.avatar = (await kubo.add(fileCandidate)).cid
    }
    return cplus
  },
  paramBuilder: cesiumPlusProfile.paramBuilder
}

/// return data handler for a query builder
const dataHandler: <T>(
  q: QueryBuilder,
  irCID: CID,
  ir: IndexRequest,
  dataCID: CID
) => (data: T) => Promise<pg.QueryResult> = (q, irCID, ir, dataCID) => (data) => {
  // console.log('adding data: ' + JSON.stringify(data))
  return client.query(q.query, q.paramBuilder(irCID, ir, dataCID, data))
}

// handle index request with non-null data
async function handleIrWithNonNullData<T>(irCID: CID, ir: IndexRequest, q: QueryBuilder): Promise<void> {
  const dataCID = ir.data
  if (dataCID == null) {
    console.log('no data when required')
    return
  }
  q.dataGetter(dataCID)
    .then((data) => q.dataTransform(irCID, ir, dataCID, data).then(dataHandler<T>(q, irCID, ir, dataCID)))
    .catch((_e) => {
      console.log('😭 error indexing ' + dataCID)
    })
}

// insert index request in database
export async function handleInsertRequest(irCID: CID, ir: IndexRequest): Promise<void> {
  // console.debug('💾 indexing ' + irCID)

  switch (ir.kind) {
    // insert cesium plus profile
    case CESIUM_PLUS_PROFILE_INSERT:
    case CESIUM_PLUS_PROFILE_INSERT_OLD:
      return handleIrWithNonNullData<CplusProfile>(irCID, ir, cesiumPlusProfile)

    // insert cesium plus import
    case CESIUM_PLUS_PROFILE_IMPORT:
      // transform base58 pubkey to ss58 address with gdev prefix
      ir.pubkey = base58ToSS58(ir.pubkey, GDEV_PREFIX)
      return handleIrWithNonNullData<CplusProfile>(irCID, ir, cesiumPlusProfileRaw)

    // delete cesium plus profile
    case CESIUM_PLUS_PROFILE_DELETE:
    case CESIUM_PLUS_PROFILE_DELETE_OLD:
      // NOTE: if delete instruction is received from past, an existing profile can be deleted
      // Cases when it can occur:
      // - a profile is deleted, then re-created, then an attacker submits again the deletion message within the MAX_IR_TIME_DIFF delay
      // - a node is synchronizing, gets profile creation from pubsub, and then gets old deletion request from peer
      // NOTE: the same can happen if create instruction if received from the past
      // We could prevent it by:
      // - keeping track of deleted profiles with a timestamp
      // Not implemented yet because these cases have low probability, but this should be implemented in the future.
      await client.query(`DELETE FROM profiles WHERE pubkey = $1;`, [ir.pubkey])
      return

    // insert transaction comment
    case TRANSACTION_COMMENT:
    case TRANSACTION_COMMENT_OLD:
      // we ignore offchain transaction comments in database for now since they happen on chain and are referenced by squid
      // in the future we could be interested to index in clear offchain tx comments value to make them searchable 
      break

    // user filesystem
    case DD_USER_FS:
      // the index request contains the data cid (non null) which is what we want to index (data is a fs that we do not look at)
      return handleIrWithNonNullData<null>(irCID, ir, userFsQb)

    // unimplemented
    default:
      console.log('🔴 unimplemented kind ' + ir.kind)
      return
  }
}

// ----------------
// utils

import { encodeAddress } from '@polkadot/util-crypto'
import bs58 from 'bs58'
const GDEV_PREFIX = 42

function base58ToSS58(publicKeyBase58: string, networkPrefix: number): string {
  // Decode the base58 public key to a Uint8Array
  // Encode the address using the provided network prefix
  return encodeAddress(bs58.decode(publicKeyBase58), networkPrefix)
}
