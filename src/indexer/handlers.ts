import type { DiffData } from './types'
import { CID } from 'multiformats'
import EventEmitter from 'events'
import { getLatestIndexedCID, handleInsertRequest, setLatestIndexedCID } from './database'
import { kubo } from '../kubo'
import { getDiff } from '../interface'
import type { IndexRequest } from '../types'
import { arrayToVinode, mergeInodesSyncCID, timestampToKey } from '../processor'
import { DD_TAMT_OPT } from './ipns'
import { setTimeout } from 'timers/promises'

// config

// size of batch insert in tree
const BATCH_SIZE = 1000
// max concurrent index requests fetches
const MAX_PARALLEL_IR_FETCH = 500
// max process queue size for synchronisation
const MAX_PROCESS_QUEUE = 2000

// === GLOBALS ===

// root CID of tree
export const GLOB_root: { cid: CID } = { cid: undefined! }
// queue of index requests waiting to be processed (added to tree then indexed)
// these are the requests received by the network
export const GLOB_processQueue: Array<[CID, IndexRequest]> = []
// lock to avoid triggering multiple simultaneous edits
let GLOB_lockTree: boolean = false
// queue of index requests waiting to be added to the tree
// these are the requests coming from other pods
const GLOB_mergeQueue: Array<[string, CID]> = []
// queue of DiffData waiting to be processed
const GLOB_diffQueue: Array<DiffData> = []
// lock to prevent multiple database edits which would fake the last root cid
// this prevents from overwriting latest indexed data
let GLOB_isIndexing = false

// === EVENTS ===

// events to communicate between processes
export const events = new EventEmitter()

// event type
export enum evtype {
  // event to trigger collecting new index requests in queue
  triggerProcess = 'triggerProcess',
  // event to trigger collecting peer index requests from queue
  triggerCollect = 'triggerCollect',
  // event to trigger injestion of new requests in database
  triggerIndex = 'triggerIndex',
  // event to trigger computing diff
  triggerComputeDiff = 'triggerComputeDiff'
}

// === HANDLERS ===

/// ----- pubsub message handler -----
export async function validMessageHandler(_cid: CID, ir: IndexRequest): Promise<void> {
  // store the index request locally (it is safe because of checks in collector)
  kubo.dag
    .put(ir)
    .then((cid) => {
      // cids of index request should be the same as the one submitted in pubsub
      // however, only the final index request cid is taken into account for safety
      if (cid.toString() != _cid.toString()) console.log('👾 ', cid, '!=', _cid)
      console.log('adding valid index request to process queue')
      // asynchronously pin the index request we just added (non recursive at this point)
      kubo.pin.add(cid).catch(() => console.log(`📌📌 could not pin index request that we just added ${cid}`))
      // add index request to the process list
      GLOB_processQueue.push([cid, ir])
      // ask to process the request
      events.emit(evtype.triggerProcess)
    })
    .catch(() => console.log(`📌 could not add valid index request ${_cid}`))
  return
}

/// insert a batch of index requests in tree
async function insertBatch(rootCID: CID, items: Array<[CID, IndexRequest]>): Promise<DiffData> {
  // convert it to a list of [key, cid] for batch insert (merge)
  const requests = items.map(([cid, ir]) => [timestampToKey(ir.time), cid]).sort() as Array<[string, CID]>
  const tree = arrayToVinode('', requests)
  // insert them
  const [_ctx, newCID] = await mergeInodesSyncCID(rootCID, tree)
  const diffData: DiffData = { oldCID: rootCID, newCID, newItems: items }
  return diffData
}

/// ----- queue event handler -----
export async function takeFromProcessQueue(): Promise<void> {
  // ignore event if already processing something or if queue is empty
  if (GLOB_lockTree) {
    // console.log('busy on tree')
    return
  }
  if (!GLOB_processQueue.length) {
    return
  }
  // if not processing, do lock process
  GLOB_lockTree = true
  // take elements from queue
  let i = undefined
  const items: Array<[CID, IndexRequest]> = []
  let num = 0
  while ((i = GLOB_processQueue.shift()) != undefined && num < BATCH_SIZE) {
    num += 1
    items.push(i)
  }
  // try inserting items
  try {
    // insert batch and get diff
    // console.log('inserting', items.length, 'items to tree')
    const diffData = await insertBatch(GLOB_root.cid, items)
    GLOB_diffQueue.push(diffData)
    const newCID = diffData.newCID
    // root CID could be untouched if processed data is already in tree
    if (GLOB_root.cid.toString() != newCID.toString()) console.log(`👉 new root CID ${newCID}`)
    // update root cid and publishes it
    GLOB_root.cid = newCID
    kubo.name.publish(newCID, DD_TAMT_OPT).catch(console.log)
  } catch (e) {
    console.error(`🥊 error merging ${GLOB_root.cid} with ${items.length} items`, e)
    GLOB_processQueue.push(...items) // add them back to the process queue
  }
  GLOB_lockTree = false
  checkQueues()
  return // nothing
}

// index diff item from diff queue to database
export async function takeFromDiffQueue(): Promise<void> {
  // ignore event if already indexing something or if queue is empty
  if (GLOB_isIndexing) {
    // console.log('busy on indexing')
    return
  }
  if (!GLOB_diffQueue.length) {
    return
  }
  // else lock
  GLOB_isIndexing = true
  // take diff
  const diff: DiffData = GLOB_diffQueue.shift()! // we know that the queue is not empty
  const latestCID = (await getLatestIndexedCID())! // (must be set at node start)
  // check that last indexed CID in db match old CID of diff
  // if not, unlock and compute diff with the new CID to make sure we get all items in db
  if (diff.oldCID.toString() != latestCID.toString()) {
    console.log('🤔 db is not at diff start, computing missing diff')
    events.emit(evtype.triggerComputeDiff, diff.oldCID, diff.newCID)
  }
  // still index the diff data
  console.log('➕ indexing', diff.newItems.length, 'items to the db')
  // insert all index requests (prevents from adding too many at the same time)
  await Promise.all(diff.newItems.map((ir) => handleInsertRequest(ir[0], ir[1])))
  // write new CID as latest indexed (but more data might be needed to be really at this stage)
  if (diff.oldCID.toString() != diff.newCID.toString()) console.log(`🦐 latest db cid ${diff.newCID}`)
  await setLatestIndexedCID(diff.newCID)
  // TODO compute what db latest cid really should be
  // unlock
  GLOB_isIndexing = false
  // --- 3. check queues
  checkQueues()
}

// compute diff with remote tree and add index requests to merge queue
// TODO prevent computing diff from several peers at the same time, otherwise CIDs will be added multiple times
export async function computeDiff(fromCID: CID, toCID: CID): Promise<void> {
  try {
    // if they differ
    if (fromCID.toString() != toCID.toString()) {
      console.log(`👐 computing diff from ${fromCID} to ${toCID}`)
      // iterate over all index requests of diff
      const iterator = getDiff(fromCID, toCID)
      let num = 0
      for await (const item of iterator) {
        for (let irCID of item.leaf) {
          // add it to the process queue to be added in the tree
          GLOB_mergeQueue.push([item.key, irCID])
          num += 1
        }
        // make sure that collection is triggered regularly
        if (num > BATCH_SIZE) {
          num = 0
          events.emit(evtype.triggerCollect)
          // This is a hack to limit injestion of new data and let time to process all
          // for 100 000 documents with a batch size of 1000 and 3 seconds, it is adding 5 minutes overall
          // await setTimeout(3000) // 3 sec
        }
      }
      events.emit(evtype.triggerCollect)
    } else {
      console.log(`👌 already at ${toCID}`)
    }
  } catch (e) {
    // compare code does some assertions about the tree format
    console.log('could not compute diff from ' + fromCID + ' to ' + toCID + ' due to error: ', e)
  }
}

// takes item from merge queue, fetch index request, and put them in process queue
export async function takeFromMergeQueue() {
  // prevent process queue from growing too much
  if (GLOB_processQueue.length > MAX_PROCESS_QUEUE) {
    // make sure it is aware
    events.emit(evtype.triggerProcess)
    return
  }
  // sort merge queue by key to give a better chance of good batching
  GLOB_mergeQueue.sort() 
  // number of items retreived
  let num = 0
  // item taker
  let i = undefined
  const items: Array<Promise<[CID, IndexRequest]>> = []
  // takes items in queue but not too many
  while ((i = GLOB_mergeQueue.shift()) != undefined && num < MAX_PARALLEL_IR_FETCH) {
    const [_k, cid] = i
    num += 1
    items.push(Promise.all([cid, kubo.dag.get(cid).then((r) => r.value)]))
  }
  const awaitedItems = await Promise.all(items)
  // awaitedItems.forEach(([c, _ir]) => {
  //   // make sure to pin the index request to be able to serve its content later
  //   // note: pubsub IRs are already pinned but peers IRs might not be
  //   kubo.pin.add(c, { recursive: true }).catch((_e) => console.log(`📌 could not pin remote index request ${c}`))
  // })
  // add index requests to process queue
  GLOB_processQueue.push(...awaitedItems)
  // ask them to be processed if not already processing
  checkProcessQueue()
  // ask next batch if any
  checkMergeQueue()
}

// check queues to see if new diff came in the meanwile
function checkQueues() {
  checkQueueDiff()
  checkProcessQueue()
  checkMergeQueue()
}

// if merge queue is not empty, collect
function checkMergeQueue() {
  if (GLOB_mergeQueue.length) {
    events.emit(evtype.triggerCollect)
  }
}

// if process queue is not empty, process it
function checkProcessQueue() {
  if (!GLOB_lockTree && GLOB_processQueue.length) {
    events.emit(evtype.triggerProcess)
  }
}

// if diff came in the meanwhile, index them
function checkQueueDiff() {
  if (GLOB_diffQueue.length) {
    events.emit(evtype.triggerIndex)
  }
}

// setInterval(() => {
//   console.log(
//     'merge queue',
//     GLOB_mergeQueue.length,
//     'process queue',
//     GLOB_processQueue.length,
//     'diff queue',
//     GLOB_diffQueue.length
//   )
// }, 10000)
