import { CID } from 'multiformats'
import { KUBO_RPC, KUBO_GATEWAY } from './kubo'

// topic used for diffusion of index requests
export const TOPIC = 'ddd'

// base used for index key
export const BASE = 16

// key size can be expressed in x chars depending on base
export const KEYSIZE = (64 * Math.log(2)) / Math.log(BASE)

// empty root cid
export const EMPTY_NODE_CID = CID.parse('bafyreic3z6fetku5dpudahsne3dwbmwkddfnqxapshknel6a2i5xhiwige')

// document kind of old cesium plus profile imported in the indexer
export const CESIUM_PLUS_PROFILE_IMPORT = 'cplus_raw'
export const CESIUM_PLUS_PROFILE_INSERT = 'cplus_upsert'
export const CESIUM_PLUS_PROFILE_INSERT_OLD = 'bafkreigi5phtqpo6a2f3tx4obaja4fzevy3nyvnl4bnkcxylyqnfeowzbm' // deprecated
export const CESIUM_PLUS_PROFILE_DELETE = 'cplus_delete'
export const CESIUM_PLUS_PROFILE_DELETE_OLD = 'bafkreic5bv5ytl7zv5rh5j2bd5mw6nfrn33mxhiobgmpsiu65yjw3eeduu' // deprecated

// document kind for transaction comment
// tx comment index request is just a request to add to the tamt, not to the database
export const TRANSACTION_COMMENT = 'dd_tx_comment'
// other kind of tx comments are not decided yet
export const TRANSACTION_COMMENT_OLD = 'bafkreiegjt5mrfj2hshuw6koejdfiykq57mzjeprfckxj5zpxxtqj4qzeu' // deprecated

// index request for user public filesystem root hash
export const DD_USER_FS = 'dd_user_fs'

// ==========

// explorer resources
const EXPLORER_CID = 'bafybeidf7cpkwsjkq6xs3r6fbbxghbugilx3jtezbza7gua3k5wjixpmba'
export const EXPLORER_URL = KUBO_RPC + '/ipfs/' + EXPLORER_CID + '/#'
export function exploreUrl(cid: CID): string {
  return EXPLORER_URL + '/explore/ipfs/' + cid
}
export function gatewayUrl(cid: CID): string {
  return KUBO_GATEWAY + '/ipfs/' + cid
}
export function gatewayUrlIPNS(ipns: string): string {
  return KUBO_GATEWAY + '/ipns/' + ipns
}
