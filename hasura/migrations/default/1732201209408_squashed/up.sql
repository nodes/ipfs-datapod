
DROP table "public"."transaction_comments";

CREATE TABLE "public"."user_fs" ("pubkey" text NOT NULL, "index_request_cid" text NOT NULL, "data_cid" text NOT NULL, "time" timestamp NOT NULL, PRIMARY KEY ("pubkey") , UNIQUE ("pubkey"), UNIQUE ("index_request_cid"));COMMENT ON TABLE "public"."user_fs" IS E'User filesystem root cid';

comment on column "public"."user_fs"."pubkey" is E'ss58 address of owner';

comment on column "public"."user_fs"."index_request_cid" is E'CID of the latest index request that modified this document';

comment on column "public"."user_fs"."data_cid" is E'CID of the user filesystem root';

comment on column "public"."user_fs"."time" is E'timestamp of the latest index request that modified this document';
