

ALTER TABLE "public"."profiles" ALTER COLUMN "avatar" TYPE text;
comment on column "public"."profiles"."avatar" is E'cid of avatar';

comment on column "public"."profiles"."title" is E'title of c+ profile';

alter table "public"."profiles" drop column "created_at" cascade;

alter table "public"."profiles" drop column "updated_at" cascade;

comment on column "public"."profiles"."address" is E'base58 pubkey of profile owner';
alter table "public"."profiles" rename column "address" to "pubkey";

alter table "public"."profiles" add column "time" timestamptz
 not null;

ALTER TABLE "public"."profiles" ALTER COLUMN "time" TYPE timestamp with time zone;
comment on column "public"."profiles"."time" is E'timestamp of the index request or C+ document if imported';

alter table "public"."profiles" add column "index_request_cid" Text
 not null unique;

comment on column "public"."profiles"."index_request_cid" is E'CID of index request';

alter table "public"."profiles" add column "data_cid" text
 null;

comment on column "public"."profiles"."data_cid" is E'CID of the latest data from which this document comes from';

comment on column "public"."profiles"."index_request_cid" is E'CID of the latest index request that modified this document';

comment on column "public"."profiles"."time" is E'timestamp of the latest index request that modified this document';
