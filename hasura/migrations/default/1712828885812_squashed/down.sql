
comment on column "public"."profiles"."time" is E'timestamp of the index request or C+ document if imported';

comment on column "public"."profiles"."index_request_cid" is E'CID of index request';

comment on column "public"."profiles"."data_cid" is NULL;

-- Could not auto-generate a down migration.
-- Please write an appropriate down migration for the SQL below:
-- alter table "public"."profiles" add column "data_cid" text
--  null;

comment on column "public"."profiles"."index_request_cid" is NULL;

-- Could not auto-generate a down migration.
-- Please write an appropriate down migration for the SQL below:
-- alter table "public"."profiles" add column "index_request_cid" Text
--  not null unique;

comment on column "public"."profiles"."time" is NULL;
ALTER TABLE "public"."profiles" ALTER COLUMN "time" TYPE timestamp with time zone;

-- Could not auto-generate a down migration.
-- Please write an appropriate down migration for the SQL below:
-- alter table "public"."profiles" add column "time" timestamptz
--  not null;

alter table "public"."profiles" rename column "pubkey" to "address";
comment on column "public"."profiles"."address" is NULL;

alter table "public"."profiles" alter column "updated_at" set default CURRENT_TIMESTAMP;
alter table "public"."profiles" alter column "updated_at" drop not null;
alter table "public"."profiles" add column "updated_at" timestamptz;

alter table "public"."profiles" alter column "created_at" set default CURRENT_TIMESTAMP;
alter table "public"."profiles" alter column "created_at" drop not null;
alter table "public"."profiles" add column "created_at" timestamptz;


comment on column "public"."profiles"."title" is NULL;

comment on column "public"."profiles"."avatar" is NULL;
ALTER TABLE "public"."profiles" ALTER COLUMN "avatar" TYPE bytea;
