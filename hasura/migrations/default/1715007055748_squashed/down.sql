
comment on column "public"."transaction_comments"."time" is NULL;
ALTER TABLE "public"."transaction_comments" ALTER COLUMN "time" TYPE timestamp with time zone;

ALTER TABLE "public"."profiles" ALTER COLUMN "time" TYPE timestamp with time zone;

-- Could not auto-generate a down migration.
-- Please write an appropriate down migration for the SQL below:
-- alter table "public"."transaction_comments" add column "time" timestamptz
--  not null;


comment on column "public"."transaction_comments"."comment" is NULL;

comment on column "public"."transaction_comments"."tx_id" is NULL;

comment on column "public"."transaction_comments"."pubkey" is NULL;

DROP TABLE "public"."transaction_comments";

comment on column "public"."profiles"."pubkey" is E'base58 pubkey of profile owner';
